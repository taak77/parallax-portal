
#!/bin/bash

# any future command that fails will exit the script
set -e

# Lets write the public key of our aws instance
eval $(ssh-agent -s)
# how to skip passphrase question?
#echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
mkdir -p ~/.ssh
touch ~/.ssh/id_rsa
echo -e "$PRIVATE_KEY" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa

# disable the host key checking.
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config
echo "disableHostKeyChecking done"
# we have already setup the DEPLOYER_SERVER in our gitlab settings which is a
# comma seperated values of ip addresses.
DEPLOY_SERVERS=$DEPLOY_SERVERS_STAGE

# lets split this string and convert this into array
# In UNIX, we can use this commond to do this
# ${string//substring/replacement}
# our substring is "," and we replace it with nothing.
ALL_SERVERS=(${DEPLOY_SERVERS//,/ })
echo "ALL_SERVERS ${ALL_SERVERS}"

chmod +x ./deploy/updateAndRestart.sh
# Lets iterate over this array and ssh into each EC2 instance
# Once inside the server, run updateAndRestart.sh
for server in "${ALL_SERVERS[@]}"
do
  echo "deploying to ${server}"
  #ssh ec2-user@${server} 'export AWS_ACCESS_KEY_ID='"'$AWS_ACCESS_KEY_ID'"'; export AWS_SECRET_ACCESS_KEY='"'$AWS_SECRET_ACCESS_KEY'"'; bash' < ./deploy/updateAndRestart.sh
  ssh -v ec2-user@${server} 'export AWS_ACCESS_KEY_ID='"'$AWS_ACCESS_KEY_ID'"'; export AWS_SECRET_ACCESS_KEY='"'$AWS_SECRET_ACCESS_KEY'"'; bash'
  echo "$AWS_ACCESS_KEY_ID"
done

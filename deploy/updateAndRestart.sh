
#!/bin/bash

# any future command that fails will exit the script
set -e

echo "executing 'updateAndRestart'"
echo "$AWS_ACCESS_KEY_ID"

sudo yum update -y
which docker || (sudo yum install -y docker)
sudo service docker start
sudo usermod -aG docker ec2-user

# Stop the running "rabbitmq" container if there is one
CONTAINER_NAME="my-app"
OLD="$(docker ps --all --quiet --filter=name="$CONTAINER_NAME")"
if [ -n "$OLD" ]; then
  docker stop $OLD && docker rm $OLD
fi

REPOSITORY_URL="292219442923.dkr.ecr.us-west-2.amazonaws.com/taak77/parallax-portal"
$(AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY aws ecr get-login --no-include-email --region us-west-2)
docker pull $REPOSITORY_URL:latest
docker run -d -p 80:5000 --name $CONTAINER_NAME $REPOSITORY_URL

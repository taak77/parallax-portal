#!/bin/bash

# Error on fail
set -e

REPOSITORY_URL: 292219442923.dkr.ecr.us-west-2.amazonaws.com/taak77/parallax-portal
IMAGE_TAG=latest
IMAGE_PATH=$REPOSITORY_URL:$IMAGE_TAG

$(aws ecr get-login --no-include-email --region us-west-2)
docker build -t $IMAGE_PATH .
docker push $IMAGE_PATH

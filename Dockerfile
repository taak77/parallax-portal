FROM node:8

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package*.json ./
# Install node dependencies
RUN npm install

COPY . .

RUN npm run build --production

# Install `serve` to run the application.
RUN npm install -g serve

# Set the command to start the node server.
CMD ["serve", "-s", "build"]

# Make port 8080 available to the world outside this container
EXPOSE 5000

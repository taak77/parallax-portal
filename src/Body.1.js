import React, { PureComponent } from 'react'
import { Parallax, ParallaxLayer } from 'react-spring'
import './Body.css'

const url = (name, wrap = false) =>
  `${
    wrap ? 'url(' : ''
  }https://awv3node-homepage.surge.sh/build/assets/${name}.svg${
    wrap ? ')' : ''
  }`

export default class Body extends PureComponent {
  static propTypes = {}

  scroll = to => this.parallax.scrollTo(to)

  render() {
    return (
      <main className="body-container">
        <Parallax ref={ref => (this.parallax = ref)} pages={3}>
          {/* page 1 */}
          <ParallaxLayer
            offset={0}
            speed={1}
            style={{
              backgroundImage:
                'url(https://images.unsplash.com/photo-1511462504624-a3e78a587eb6?ixlib=rb-0.3.5&s=91b9aa02377b07e82be0f85e82066055&auto=format&fit=crop&w=2100&q=80)',
              backgroundSize: 'cover',
              backgroundColor: '#0000fc',
            }}
          />
          {/* page 2 */}
          <ParallaxLayer
            offset={1}
            speed={1}
            style={{ backgroundColor: '#805E73' }}
          />
          {/* page 3 */}
          <ParallaxLayer
            offset={2}
            speed={1}
            style={{ backgroundColor: '#87BCDE' }}
          />

          <ParallaxLayer
            offset={0}
            speed={0}
            factor={3}
            style={{
              backgroundImage: url('stars', true),
              backgroundSize: 'cover',
            }}
          />

          <ParallaxLayer
            offset={1.3}
            speed={-0.3}
            style={{ pointerEvents: 'none' }}
          >
            <img
              src={url('satellite4')}
              style={{ width: '15%', marginLeft: '70%' }}
              alt="satellite"
            />
          </ParallaxLayer>

          <ParallaxLayer offset={1} speed={0.8} style={{ opacity: 0.1 }}>
            <img
              src={url('cloud')}
              style={{
                display: 'block',
                width: '20%',
                marginLeft: '55%',
              }}
              alt="cloud"
            />
            <img
              src={url('cloud')}
              style={{
                display: 'block',
                width: '10%',
                marginLeft: '15%',
              }}
              alt="cloud"
            />
          </ParallaxLayer>

          <ParallaxLayer offset={1.75} speed={0.5} style={{ opacity: 0.1 }}>
            <img
              src={url('cloud')}
              style={{
                display: 'block',
                width: '20%',
                marginLeft: '70%',
              }}
              alt="cloud"
            />
            <img
              src={url('cloud')}
              style={{
                display: 'block',
                width: '20%',
                marginLeft: '40%',
              }}
              alt="cloud"
            />
          </ParallaxLayer>

          <ParallaxLayer offset={1} speed={0.2} style={{ opacity: 0.2 }}>
            <img
              src={url('cloud')}
              style={{
                display: 'block',
                width: '10%',
                marginLeft: '10%',
              }}
              alt="cloud"
            />
            <img
              src={url('cloud')}
              style={{
                display: 'block',
                width: '20%',
                marginLeft: '75%',
              }}
              alt="cloud"
            />
          </ParallaxLayer>

          <ParallaxLayer offset={1.6} speed={-0.1} style={{ opacity: 0.4 }}>
            <img
              src={url('cloud')}
              style={{
                display: 'block',
                width: '20%',
                marginLeft: '60%',
              }}
              alt="cloud"
            />
            <img
              src={url('cloud')}
              style={{
                display: 'block',
                width: '25%',
                marginLeft: '30%',
              }}
              alt="cloud"
            />
            <img
              src={url('cloud')}
              style={{
                display: 'block',
                width: '10%',
                marginLeft: '80%',
              }}
              alt="cloud"
            />
          </ParallaxLayer>

          <ParallaxLayer offset={2.6} speed={0.4} style={{ opacity: 0.6 }}>
            <img
              src={url('cloud')}
              style={{
                display: 'block',
                width: '20%',
                marginLeft: '5%',
              }}
              alt="cloud"
            />
            <img
              src={url('cloud')}
              style={{
                display: 'block',
                width: '15%',
                marginLeft: '75%',
              }}
              alt="cloud"
            />
          </ParallaxLayer>

          <ParallaxLayer
            offset={2.5}
            speed={-0.4}
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              pointerEvents: 'none',
            }}
          >
            <img src={url('earth')} style={{ width: '60%' }} alt="earth" />
          </ParallaxLayer>

          <ParallaxLayer
            offset={2}
            speed={-0.3}
            style={{
              backgroundSize: '80%',
              backgroundPosition: 'center',
              backgroundImage: url('clients', true),
            }}
          />

          <ParallaxLayer
            offset={0}
            speed={0.1}
            onClick={() => this.parallax.scrollTo(1)}
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <img src={url('server')} style={{ width: '20%' }} alt="server" />
          </ParallaxLayer>

          <ParallaxLayer
            offset={1}
            speed={0.1}
            onClick={() => this.parallax.scrollTo(2)}
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <img src={url('bash')} style={{ width: '40%' }} alt="bash" />
          </ParallaxLayer>

          <ParallaxLayer
            offset={2}
            speed={-0}
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onClick={() => this.parallax.scrollTo(0)}
          >
            <img
              src={url('clients-main')}
              style={{ width: '40%' }}
              alt="clients-main"
            />
          </ParallaxLayer>
        </Parallax>
      </main>
    )
  }
}

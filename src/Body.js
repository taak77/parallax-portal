import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Parallax, ParallaxLayer } from 'react-spring'
import { connect } from 'react-redux'
import styles from './Body.module.css'

class Body extends PureComponent {
  static propTypes = {
    page: PropTypes.number,
  }

  scroll = offset => this.parallax.scrollTo(offset)

  componentDidMount() {
    this.scroll(this.props.page - 1)
  }

  componentDidUpdate(prevProps) {
    if (this.props.page !== prevProps.page) {
      this.scroll(this.props.page - 1)
    }
  }

  render() {
    return (
      <main className={styles.container}>
        <Parallax ref={ref => (this.parallax = ref)} pages={4}>
          {/* page 1 */}
          <ParallaxLayer
            id="page-1"
            offset={0}
            speed={0}
            style={{
              backgroundImage:
                'url(https://images.unsplash.com/photo-1511462504624-a3e78a587eb6?ixlib=rb-0.3.5&s=91b9aa02377b07e82be0f85e82066055&auto=format&fit=crop&w=2100&q=80)',
              backgroundSize: 'cover',
              backgroundPosition: 'center',
              backgroundColor: '#000',
            }}
          />
          {/* page 2 */}
          <ParallaxLayer
            id="page-2"
            offset={1}
            speed={0}
            style={{
              backgroundColor: '#fff',
            }}
          />
          {/* page 2 */}
          <ParallaxLayer
            id="page-3"
            offset={2}
            speed={0}
            style={{
              backgroundImage:
                'url(https://images.unsplash.com/photo-1495422964407-28c01bf82b0d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b2907488bc3f70cce5eaea02c7e77a0f&auto=format&fit=crop&w=2100&q=80)',
              backgroundSize: 'cover',
              backgroundPosition: 'center',
              backgroundColor: '#000',
            }}
          />
          {/* page 3 */}
          <ParallaxLayer
            id="page-4"
            offset={3}
            speed={0}
            style={{
              backgroundImage:
                'url(https://images.unsplash.com/photo-1451976426598-a7593bd6d0b2?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=47863721b85245e8153d06c1a47649a0&auto=format&fit=crop&w=2100&q=80)',
              backgroundSize: 'cover',
              backgroundPosition: 'center',
              backgroundColor: '#000',
            }}
          />

          <ParallaxLayer
            offset={0}
            speed={1}
            onClick={() => this.parallax.scrollTo(1)}
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <div
              style={{
                marginTop: 60,
              }}
            >
              <h2 className={styles.h2}>This is Page 1</h2>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin
                ut dolor iaculis, rutrum justo id, volutpat diam. Pellentesque
                vel porta elit. Donec placerat massa tortor, vel facilisis nisl
                ullamcorper vitae. Nam eget lacus congue, lobortis felis et,
                cursus ex. Nullam vestibulum sed sapien vel commodo. Proin
                vestibulum quam sapien, vel ultrices quam porta et. Ut nec
                dictum dui, vitae dignissim lorem. Nam vel tincidunt erat.
                Quisque faucibus porttitor quam vitae lacinia. Sed mauris justo,
                pulvinar non ante et, pulvinar laoreet dolor. Cras varius, odio
                a hendrerit pulvinar, purus ex suscipit ante, a scelerisque
                justo erat non ex. Phasellus pellentesque lacus vel vulputate
                tristique. Pellentesque habitant morbi tristique senectus et
                netus et malesuada fames ac turpis egestas.
              </p>
            </div>
          </ParallaxLayer>

          <ParallaxLayer
            offset={1}
            speed={1}
            onClick={() => this.parallax.scrollTo(2)}
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <div
              style={{
                marginTop: 60,
                color: 'rgb(196, 100, 164)',
              }}
            >
              <h2 className={styles.h2}>This is Page 2</h2>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin
                ut dolor iaculis, rutrum justo id, volutpat diam. Pellentesque
                vel porta elit. Donec placerat massa tortor, vel facilisis nisl
                ullamcorper vitae. Nam eget lacus congue, lobortis felis et,
                cursus ex. Nullam vestibulum sed sapien vel commodo. Proin
                vestibulum quam sapien, vel ultrices quam porta et. Ut nec
                dictum dui, vitae dignissim lorem. Nam vel tincidunt erat.
                Quisque faucibus porttitor quam vitae lacinia. Sed mauris justo,
                pulvinar non ante et, pulvinar laoreet dolor. Cras varius, odio
                a hendrerit pulvinar, purus ex suscipit ante, a scelerisque
                justo erat non ex. Phasellus pellentesque lacus vel vulputate
                tristique. Pellentesque habitant morbi tristique senectus et
                netus et malesuada fames ac turpis egestas.
              </p>
            </div>
          </ParallaxLayer>

          <ParallaxLayer
            offset={2}
            speed={1}
            onClick={() => this.parallax.scrollTo(3)}
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <div
              style={{
                marginTop: 60,
              }}
            >
              <h2 className={styles.h2}>This is Page 3</h2>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin
                ut dolor iaculis, rutrum justo id, volutpat diam. Pellentesque
                vel porta elit. Donec placerat massa tortor, vel facilisis nisl
                ullamcorper vitae. Nam eget lacus congue, lobortis felis et,
                cursus ex. Nullam vestibulum sed sapien vel commodo. Proin
                vestibulum quam sapien, vel ultrices quam porta et. Ut nec
                dictum dui, vitae dignissim lorem. Nam vel tincidunt erat.
                Quisque faucibus porttitor quam vitae lacinia. Sed mauris justo,
                pulvinar non ante et, pulvinar laoreet dolor. Cras varius, odio
                a hendrerit pulvinar, purus ex suscipit ante, a scelerisque
                justo erat non ex. Phasellus pellentesque lacus vel vulputate
                tristique. Pellentesque habitant morbi tristique senectus et
                netus et malesuada fames ac turpis egestas.
              </p>
            </div>
          </ParallaxLayer>

          <ParallaxLayer
            offset={3}
            speed={1}
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
            onClick={() => this.parallax.scrollTo(0)}
          >
            <div
              style={{
                marginTop: 60,
              }}
            >
              <h2 className={styles.h2}>This is Page 4</h2>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin
                ut dolor iaculis, rutrum justo id, volutpat diam. Pellentesque
                vel porta elit. Donec placerat massa tortor, vel facilisis nisl
                ullamcorper vitae. Nam eget lacus congue, lobortis felis et,
                cursus ex. Nullam vestibulum sed sapien vel commodo. Proin
                vestibulum quam sapien, vel ultrices quam porta et. Ut nec
                dictum dui, vitae dignissim lorem. Nam vel tincidunt erat.
                Quisque faucibus porttitor quam vitae lacinia. Sed mauris justo,
                pulvinar non ante et, pulvinar laoreet dolor. Cras varius, odio
                a hendrerit pulvinar, purus ex suscipit ante, a scelerisque
                justo erat non ex. Phasellus pellentesque lacus vel vulputate
                tristique. Pellentesque habitant morbi tristique senectus et
                netus et malesuada fames ac turpis egestas.
              </p>
            </div>
          </ParallaxLayer>
        </Parallax>
      </main>
    )
  }
}

const mapStateToProps = state => {
  return {
    page: state.page,
  }
}

export default connect(mapStateToProps)(Body)

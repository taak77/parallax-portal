export function route(pageId) {
  return {
    type: 'ROUTE',
    page: parseInt(pageId, 10),
  }
}

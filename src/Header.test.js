import React from 'react'
import Enzyme, { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import configureStore from 'redux-mock-store'
import ConnectedHeader from './Header'

Enzyme.configure({ adapter: new Adapter() })

describe('Header', () => {
  const initialState = {
    title: 'My Website',
    pages: [
      { id: 1, title: 'Page 1' },
      { id: 2, title: 'Page 2' },
      { id: 3, title: 'Page 3' },
      { id: 4, title: 'Page 4' },
    ],
    page: 1,
  }
  const mockStore = configureStore()
  let store
  let container

  beforeEach(() => {
    store = mockStore(initialState)
    container = shallow(<ConnectedHeader store={store} />)
  })

  it('render the connected component', () => {
    expect(container.length).toEqual(1)
  })

  it('check Prop matches with initialState', () => {
    expect(container.prop('title')).toEqual(initialState.title)
    expect(container.prop('pages')).toEqual(initialState.pages)
  })

  it('check Prop matches with initialState', () => {
    container = mount(<ConnectedHeader store={store} />)
    expect(container.find('ul').children().length).toEqual(
      initialState.pages.length
    )
  })
})

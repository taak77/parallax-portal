import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'
import createHistory from 'history/createBrowserHistory'
import './index.css'
import App from './App'

const reducer = (state, action) => {
  switch (action.type) {
    case 'INIT':
    case 'ROUTE':
      return {
        ...state,
        page: action.page,
      }
    default:
      return state
  }
}

const history = createHistory({
  basename: '/page',
})

let initialPage = history.location.pathname.split('/').pop()
initialPage = parseInt(initialPage, 10) || 1
const initialState = {
  title: 'My Website 2',
  pages: [
    { id: 1, title: 'Page 1' },
    { id: 2, title: 'Page 2' },
    { id: 3, title: 'Page 3' },
    { id: 4, title: 'Page 4' },
  ],
  page: initialPage,
}

function locationMiddleware() {
  return next => action => {
    const returnValue = next(action)
    if (action.type == 'INIT' || action.type == 'ROUTE') {
      history.push(`/${action.page}`)
    }

    return returnValue
  }
}

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      })
    : compose

const enhancer = composeEnhancers(
  applyMiddleware(locationMiddleware)
  // other store enhancers if any
)

const store = createStore(reducer, initialState, enhancer)

store.dispatch({
  type: 'INIT',
  page: initialPage,
})

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)

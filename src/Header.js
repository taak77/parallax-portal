import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styles from './Header.module.css'
import { route } from './actions'

export class Header extends PureComponent {
  static propTypes = {
    dispatch: PropTypes.func,
    title: PropTypes.string,
    pages: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
      })
    ),
  }

  onClick = event => {
    this.props.dispatch(route(event.target.getAttribute('data-page-index')))
  }

  render() {
    const { title, pages } = this.props

    const links = pages.map(({ id, title }) => {
      return (
        <li key={id}>
          <a
            href="javascipt:void(0)"
            data-page-index={id}
            onClick={this.onClick}
          >
            {title}
          </a>
        </li>
      )
    })
    return (
      <header className={styles.container}>
        <h2 className={styles.title}>{title}</h2>
        <div className={styles.spacer} />
        <ul>{links}</ul>
      </header>
    )
  }
}

const mapStateToProps = ({ title, pages }) => ({
  title,
  pages,
})

export default connect(mapStateToProps)(Header)
